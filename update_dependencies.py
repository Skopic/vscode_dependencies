import os
import json

"""Creates a python virtual environment for a project, and copies all of its dependencies from a 
Docker container to to venv. This environment can then (only!) be used by Visual Studio Code to select the 
Python interpreter from and load all dependencies, so goto-definition works properly."""


settings = json.loads(open('settings.json', 'r').read())
venvs_path = settings.get('python.venvPath')
project_name = os.path.basename(os.path.abspath('..'))
container_python_path = '/usr/local/lib/python3.7/site-packages'
local_dependencies_path = venvs_path + project_name + "/Lib"

print("Updating Visual Studio Code dependencies for this project - {}".format(project_name))

if not os.path.exists(venvs_path+project_name):
    print("Virtual environment does not exist for this project")
    print("Creating virtual environment...")
    os.popen('python -m venv {}{}'.format(venvs_path, project_name)).read()
else:
    print("Virtual environment already exists for this project")


container_name = input("Please enter the name of the service running Python: ")
print("Copying dependencies to local venv...")
container_id = os.popen('docker-compose ps -q {}'.format(container_name)).read().rstrip()

command = "docker cp {}:{} {}".format(container_id, 
                                      container_python_path, 
                                      local_dependencies_path)
os.popen(command).read()

print("Finished. You may have to restart Visual Studio Code for the changes to take effect.")
print("Dont forget to set the Python interpreter for this project (ctrl+shift+p)-> Select Interpreter")
