# vscode_dependencies

Makes goto-definition work for Dockerized python projects

## How it works
The script creates a Python virtual environment for the project you are working in, and then copies all of the packages installed in the container you've 
specified to this virtual environment. After this, Visual Studio Code will recognize the new virtual environment and you are then able to select the Python 
interpreter from it. 

## Usage
1. In your Visual Studio Code user settings, set the ```python.venvPath``` setting to a directory where you
   would like to store the virtual envs.
2. Place ```update_dependencies.py``` in your projects ```.vscode/``` directory (where ```settings.py``` resides as well).
3. Make sure your ```code-runner.runInTerminal``` is set to ```true```.
4. Run the script and follow the instructions.
5. Restart Visual Studio Code and select the Python interpreter using ```ctrl+shift+p``` -> ```Python: Select Interpreter```
